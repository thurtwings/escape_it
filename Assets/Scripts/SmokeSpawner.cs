﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class SmokeSpawner : MonoBehaviour
    {
        #region Variables
        public GameObject[] smokePuffs;
        public float timeLeft = 0.1f;
         
	#endregion

        void Start()
        {
            
        }
        private void Update()
        {
            timeLeft -= Time.deltaTime;
            if(timeLeft <= 0)
            {
                Spawn();
            }
        }

        void Spawn()
        {
            timeLeft = Random.Range(0.1f, 1f);
            GameObject cloudToInstantiate = Instantiate(smokePuffs[Random.Range(1, smokePuffs.Length)], transform.position, Quaternion.Euler(0,0,90), transform);
            Destroy(cloudToInstantiate, 15f);
        }

        
    }
}