﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ThurtwingsGames
{
    public class TutoResume : MonoBehaviour
    {
        #region Variables

        public GameObject tutoScreen;
        public GameObject timerBar;
        public GameObject timerWarning;

        #endregion

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                StartCoroutine(Warning());
                timerBar.SetActive(true);
                timerWarning.SetActive(true);
                tutoScreen.SetActive(false);
                
            }
        }

        IEnumerator Warning()
        {
            yield return new WaitForSeconds(5f);
            timerWarning.SetActive(false);
            Destroy(this.gameObject);
        }
    }
}