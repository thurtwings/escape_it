﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace ThurtwingsGames
{
    public class TriggerTuto : MonoBehaviour
    {
        #region Variables
        //[SerializeField] GameObject textToShow;
        [TextArea(2,5)]
        public string tutorialSentence;
        public TMP_Text tutoText;
        #endregion

        

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                
                tutoText.text = tutorialSentence;
            }
        }
        
    }
}