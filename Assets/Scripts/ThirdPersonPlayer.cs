﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    // TODO: modif du saut : plus on appuis, plus on saute haut
    // TODO: ajourter un raycast devant la souris pour détécter les cartons et jouer le son
    public class ThirdPersonPlayer : MonoBehaviour
    {
        public static ThirdPersonPlayer instance;
        #region Variables
        [Header("Basics")]
        public Rigidbody rb;
        public float moveSpeed, jumpForce, originalSpeed;
        public bool isGrounded, isJumping, isSprinting;
        private Vector3 velocity = Vector3.zero;
        public Transform groundCheckPoint;
        float horizontalMovement;
        public float gravityMultiplier, pullMultiplier;
        public bool facingLeft;
        public bool facingRight;
        public Animator animator;
        public float rayLenght;
        public LayerMask ObjThatCrush;
        [Header("Capacities")]
        public bool hasTool;
        public Transform toolPlace;
        public GameObject ratToSplash;
        #endregion
        private void Awake()
        {
            instance = this;
        }
        void Start()
        {
            rb = GetComponent<Rigidbody>();
            facingLeft = false;
            facingRight = true;
            isSprinting = false;
            hasTool = false;
            animator = animator.GetComponent<Animator>();
            transform.position = RespawnManager.instance.lastCheckPointPosition;
        }

        private void Update()
        {
            
            if (Input.GetButtonDown("Jump") && isGrounded && !isJumping )
            {
                isJumping = true;
                animator.SetTrigger("Jump");
            }
            if (Input.GetKey(KeyCode.LeftShift) && !Input.GetMouseButton(1))
            {
                
                moveSpeed = 450f;
                
            }
            else if (Input.GetMouseButton(1))
            {
                moveSpeed = 150f;
            }
            else
            {
                
                moveSpeed = originalSpeed;
            }
            if (facingLeft)
            {
                
                this.gameObject.transform.rotation = Quaternion.Euler(0, 270, 0);
            }
            if (facingRight)
            {
                
                this.gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
            }
            if (!hasTool)
            {
                if (toolPlace.transform.childCount > 0)
                {
                    GameObject objToDestroy = toolPlace.transform.GetChild(0).gameObject;
                    Destroy(objToDestroy);
                }
                else { return; }
            }
        }
        void FixedUpdate()
        {
            horizontalMovement = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;
            RaycastHit hit;
            

            Debug.DrawRay(transform.position + new Vector3(0, .5f, 0), new Vector3(0, 0.1f, 0), Color.red);
            
            MovePlayer(horizontalMovement);
            if (rb.velocity.y < 0f)
            {
                rb.velocity += (-Vector3.up * gravityMultiplier);
            }
            if (rb.velocity.y > 0f && !isJumping)
            {
                rb.velocity += (-Vector3.up * pullMultiplier);
            }
            if (Physics.Raycast((transform.position + new Vector3(0, .5f, 0)), new Vector3(0, 0.1f, 0), out hit, rayLenght, ObjThatCrush))
            {
                Debug.Log(hit.transform.gameObject.name);
                if (!hit.transform.gameObject.GetComponent<Rigidbody>())
                {
                    return;
                }
                if (hit.transform.gameObject.GetComponent<Rigidbody>().mass >= 10f )
                {
                    if(hit.transform.gameObject.GetComponent<Rigidbody>().velocity.y > 1f)
                    {
                        ratToSplash.transform.localScale = new Vector3(ratToSplash.transform.localScale.x, 0.1f, ratToSplash.transform.localScale.z);
                        StartCoroutine(GameManager.instance.WaitToDieSplatched());
                    }
                    
                }
                else { return; }
            } 
        }
        private void OnTriggerStay(Collider collision)
        {
            if (collision.gameObject.CompareTag("Ground"))
            {
                isGrounded = true;
                
            }

        }
        private void OnTriggerExit(Collider collision)
        {
            isGrounded = false;
        }

        public void MovePlayer(float horizontalMovement)
        {
            Vector3 lTargetVelocity = new Vector3(horizontalMovement, rb.velocity.y, 0f);
            rb.velocity = Vector3.SmoothDamp(rb.velocity, lTargetVelocity, ref velocity, .05f);
            



            if(horizontalMovement < -4f)
            {
                facingLeft = true;
                facingRight = false;
                animator.SetFloat("Move", 1f);
                animator.SetBool("Idle", false);

            }
            else if (horizontalMovement > 4f)
            {
                facingRight = true;
                facingLeft = false;
                animator.SetFloat("Move", 1f);
                animator.SetBool("Idle", false);

            }
            else if(horizontalMovement < -0.15f && horizontalMovement > -3.5f)
            {
                facingLeft = true;
                facingRight = false;
                animator.SetFloat("Move", .5f);
                animator.SetBool("Idle", false);
            }
            else if(horizontalMovement > 0.15f && horizontalMovement < 3.5f)
            {
                facingRight = true;
                facingLeft = false;
                animator.SetFloat("Move", .5f);
                animator.SetBool("Idle", false);
            }
            else
            {
                animator.SetBool("Idle", true);
                animator.SetFloat("Move", 0f);
            }

            if (isJumping)
            {
                rb.AddForce(0f, jumpForce, 0f);
                isJumping = false;
            }

        }


        
    }

}
