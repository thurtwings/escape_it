﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace ThurtwingsGames
{
    public class CreditText : MonoBehaviour
    {
        #region Variables
        public TextMeshProUGUI credits;
        [TextArea(5,15)]
        public string creditLines;
        public float typingSpeed;


	#endregion
        
        void Start()
        {
            StartCoroutine(Type());
        }

        IEnumerator Type()
        {
            yield return new WaitForSeconds(1f);
            foreach (char letter in creditLines)
            {
                credits.text += letter;
                yield return new WaitForSeconds(typingSpeed);

            }
        }
        void Update()
        {
            
        }
    }
}