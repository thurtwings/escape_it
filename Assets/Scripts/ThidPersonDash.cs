﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class ThidPersonDash : MonoBehaviour
    {
        #region Variables
        ThirdPersonPlayer moveScript;
        public static ThidPersonDash instance;
        public float dashSpeed;
        public float camShakeIntensity;
        public float dashDuration;
        public bool isDashing = false;
        public bool needRest = false;
        public GameObject trail;
        AudioSource dashSound;
        Vector3 mousePos;
        #endregion
        private void Awake()
        {
            instance = this;
        }
        void Start()
        {
            moveScript = GetComponent<ThirdPersonPlayer>();
            dashSound = GetComponent<AudioSource>();
        }

    
        void Update()
        {
            if (Input.GetMouseButtonDown(0) && !isDashing && !needRest)
            {
                
                isDashing = true;
                StartCoroutine(Dash());
            }

        }

        public IEnumerator Dash()
        {
            //mousePos = Input.mousePosition;
            //Vector3 mousePosGlobal = Camera.main.ScreenToWorldPoint(mousePos);
            //Debug.Log((mousePosGlobal - transform.position).normalized);
            //Debug.Log("mouseGlobalPos : " + mousePosGlobal);
            ThirdPersonPlayer.instance.animator.SetTrigger("Dash");
            dashSound.Play();
            float startTime = Time.time;
            while(Time.time < startTime + dashDuration)
            {
                CineShake.instance.ShakeCam(camShakeIntensity, dashDuration);
                trail.SetActive(true);
                if (moveScript.facingRight)
                {
                    moveScript.rb.AddForce(new Vector3(dashSpeed, 0f, 0f) * Time.deltaTime, ForceMode.Impulse);
                    //moveScript.rb.AddForce((mousePosGlobal - transform.position).normalized *dashSpeed* Time.deltaTime, ForceMode.Impulse);
                }
                else if(moveScript.facingLeft)
                {
                    moveScript.rb.AddForce(new Vector3(-dashSpeed, 0f, 0f) * Time.deltaTime, ForceMode.Impulse);
                    //moveScript.rb.AddForce(new Vector3(mousePos.x, mousePos.y, 0) * Time.deltaTime, ForceMode.Impulse);
                }
                
                yield return null;
                
                needRest = true;
                

            }
            trail.SetActive(false);
            isDashing = false;
            yield return new WaitForSeconds(1f);
            needRest = false;
            

        }
    }
}