﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ThurtwingsGames
{
    public class UIManager : MonoBehaviour
    {
        #region Variables
        [SerializeField] TMP_Text tutoText;
	#endregion

        void Start()
        {
            tutoText.gameObject.SetActive(false);
        }

        public void StartTuto()
        {
            tutoText.gameObject.SetActive(true);
            tutoText.text = "Use A and D or the left and right arrows to move left and right. \n Use the space bar to jump. \nUse the left button of the mouse to dash.";
        }
    }
}