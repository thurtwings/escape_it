﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ThurtwingsGames
{
    public class GameManager : MonoBehaviour
    {
        #region Variables
        public static GameManager instance;
        public GameObject AcidGameOverPanel;
        public GameObject SpiderGameOverPanel;
        public GameObject TimeGameOverPanel;
        public GameObject SplatchGameOverPanel;
        public GameObject winPanel;
        public GameObject pausePanel;
        public GameObject player;
        public GameObject credits;
        public GameObject tutoScreen;
        public GameObject timerBar;
        bool clickedCredits = false;
        public bool isPaused = false;
        public bool hasWon = false;
        AudioSource audioSource;
        public RespawnManager rpmanager;
	    #endregion

        void Awake()
        {
            if(instance == null)
            {
                instance = this;
                
            }
            
            else 
            { 
                Destroy(gameObject); 
            }
            
            timerBar.SetActive(false);
            //instance = this;
            AcidGameOverPanel.SetActive(false);
            SpiderGameOverPanel.SetActive(false);
            TimeGameOverPanel.SetActive(false);
            SplatchGameOverPanel.SetActive(false);
            winPanel.SetActive(false);
            pausePanel.SetActive(false);
            credits.SetActive(false);
            tutoScreen.SetActive(true);
            Time.timeScale = 1f;
            audioSource = GetComponent<AudioSource>();

            if(rpmanager.timerLeftLevel > 0)
            {
                timerBar.SetActive(true);
                TimerLevel.instance.timeLeft = rpmanager.timerLeftLevel;
                tutoScreen.SetActive(false);
            }
        }
        private void Start()
        {
            rpmanager = FindObjectOfType<RespawnManager>();
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Pause();
            }
        }

        public void GameOverByAcid()
        {
            audioSource.Play();
            StartCoroutine(AcidWaitToDie());
        }
        public void GameOverBySpider()
        {
            audioSource.Play();
            StartCoroutine(SpiderWaitToDie());
        }
        public void GameOverByTimer()
        {
            audioSource.Play();
            StartCoroutine(WaitToDieTimer());
        }
        public void GameOverSplatched()
        {
            audioSource.Play();
            StartCoroutine(WaitToDieSplatched());
        }
        public void Win()
        {
            hasWon = true;
            winPanel.SetActive(true);
            player.GetComponent<ThirdPersonPlayer>().enabled = false;
            player.GetComponent<ThidPersonDash>().enabled = false;
            player.GetComponent<Rigidbody>().velocity = Vector3.zero;
            timerBar.SetActive(false);

            
        }
        public void RetryButton()
        {
            
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void QuitButton()
        {
            Application.Quit();
        }

        public void Pause()
        {
            isPaused = !isPaused;
            if (isPaused)
            {
                pausePanel.SetActive(true);
                player.GetComponent<ThirdPersonPlayer>().enabled = false;
                player.GetComponent<ThidPersonDash>().enabled = false;
                Time.timeScale = 0f;
                
                
            }
            else
            {
                Time.timeScale = 1f;
                player.GetComponent<ThirdPersonPlayer>().enabled = true;
                player.GetComponent<ThidPersonDash>().enabled = true;
                pausePanel.SetActive(false);
                
                
            }
        }
        public void CreditButton()
        {
            clickedCredits = !clickedCredits;

            if (clickedCredits)
            {
                credits.SetActive(true);
                
            }

            else 
            { 
                credits.SetActive(false);
                
            }
        }

        public IEnumerator AcidWaitToDie()
        {
            ThirdPersonPlayer.instance.animator.SetTrigger("Death");
            player.GetComponent<ThirdPersonPlayer>().enabled = false;
            player.GetComponent<ThidPersonDash>().enabled = false;
            yield return new WaitForSeconds(2f);
            AcidGameOverPanel.SetActive(true);
            yield return new WaitForSeconds(2f);
            Time.timeScale = 0f;
        }
        public IEnumerator SpiderWaitToDie()
        {
            ThirdPersonPlayer.instance.animator.SetTrigger("Death");
            player.GetComponent<ThirdPersonPlayer>().enabled = false;
            player.GetComponent<ThidPersonDash>().enabled = false;
            yield return new WaitForSeconds(2f);
            SpiderGameOverPanel.SetActive(true);
            yield return new WaitForSeconds(2f);
            Time.timeScale = 0f;
        }
        public IEnumerator WaitToDieTimer()
        {
            ThirdPersonPlayer.instance.animator.SetTrigger("Death");
            player.GetComponent<ThirdPersonPlayer>().enabled = false;
            player.GetComponent<ThidPersonDash>().enabled = false;
            yield return new WaitForSeconds(2f);
            TimeGameOverPanel.SetActive(true);
            yield return new WaitForSeconds(2f);
            Time.timeScale = 0f;


        }
        public IEnumerator WaitToDieSplatched()
        {
            ThirdPersonPlayer.instance.animator.SetTrigger("Death");
            player.GetComponent<ThirdPersonPlayer>().enabled = false;
            player.GetComponent<ThidPersonDash>().enabled = false;
            yield return new WaitForSeconds(2f);
            SplatchGameOverPanel.SetActive(true);
            yield return new WaitForSeconds(2f);
            Time.timeScale = 0f;


        }
    }
}