﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace ThurtwingsGames
{

    public class CineShake : MonoBehaviour
    {

        #region Variables
        public static CineShake instance;
        CinemachineVirtualCamera cM_Cam;
        private float shakeTimer;
        #endregion
        private void Awake()
        {
            instance = this;
            cM_Cam = GetComponent<CinemachineVirtualCamera>();
            
        }
        
        public void ShakeCam(float intensity, float duration)
        {
            CinemachineBasicMultiChannelPerlin shakeNoise = 
                cM_Cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            shakeNoise.m_AmplitudeGain = intensity;
            shakeTimer = duration;
        }

        void Update()
        {
            if(shakeTimer > 0f)
            {
                shakeTimer -= Time.deltaTime;
                if(shakeTimer <= 0f)
                {
                    CinemachineBasicMultiChannelPerlin shakeNoise =
                cM_Cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                    shakeNoise.m_AmplitudeGain = 0f;
                }
            }
        }
    }
}