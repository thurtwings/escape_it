﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class RespawnManager : MonoBehaviour
    {
        #region Variables
        public static RespawnManager instance;
        public Vector3 lastCheckPointPosition;
        public float timerLeftLevel;
        public GameObject originalPos;
        #endregion


        private void Awake()
        {
            if(instance == null)
            {
                instance = this;
                DontDestroyOnLoad(instance);
            }
            else
            {
                Destroy(gameObject);
            }
            lastCheckPointPosition = this.transform.position;
            
            
        }
        void Start()
        {
            
        }

    
        void Update()
        {
            
           
            if (GameManager.instance.hasWon)
            {
                lastCheckPointPosition = originalPos.transform.position;
                
            }
            
        }
    }
}