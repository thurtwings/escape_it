﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class Win : MonoBehaviour
    {
        #region Variables
        Animator animator;
	#endregion

        void Start()
        {
            animator = GetComponent<Animator>();
        }

    
        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                GameManager.instance.Win();
            }
        }
    }
}