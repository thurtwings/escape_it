﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ThurtwingsGames
{
    public class Spider : MonoBehaviour
    {
        #region Variables
        public LayerMask playerMask;
        public bool attackPlayer = false;
        public bool catchPlayer = false;
        public bool touchedGround = false;
        public float speed;
        public GameObject playersBlood;
        public GameObject playersBloodParticle;
        private Vector3 initialPosition;
        public  Vector3 offset;
        Rigidbody spiderRb;
        AudioSource audioSource;
        [SerializeField] AudioClip[] sounds;

        #endregion

        private void Start()
        {
            initialPosition = transform.position;
            spiderRb = GetComponent<Rigidbody>();
            audioSource = GetComponent<AudioSource>();
        }

        void Update()
        {
            if (attackPlayer)
            {
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
                
            }
            if (catchPlayer)
            {
                
                attackPlayer = false;
                transform.Translate(-Vector3.forward * speed / 4 * Time.deltaTime);
                
                playersBlood.transform.parent = null;
                playersBlood.transform.localScale += new Vector3(.2f, .2f, .2f) * Time.deltaTime;
                playersBlood.transform.position = new Vector3(playersBlood.transform.position.x, .51f, playersBlood.transform.position.z);
                playersBloodParticle.SetActive(true);
                
            }
            if (touchedGround && transform.position.y < initialPosition.y)
            {
                transform.Translate(-Vector3.forward * speed / 2 * Time.deltaTime);
            }
            RaycastHit hit;
            if (Physics.Raycast(transform.position, new Vector3(0, -20, 0), out hit, playerMask))
            {
                if(hit.transform.gameObject.name == "Player")
                {
                    attackPlayer = true;
                    
                }
                
            }


            Debug.DrawRay(transform.position, new Vector3(0, -20, 0), Color.red);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                ThirdPersonPlayer.instance.animator.SetTrigger("DeathSpider");
                other.gameObject.GetComponent<ThirdPersonPlayer>().enabled = false;
                other.gameObject.GetComponent<ThidPersonDash>().enabled = false;
                other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                //other.gameObject.GetComponent<Rigidbody>().useGravity = false;
                other.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
                other.transform.parent = this.transform;
                other.transform.position  = this.transform.position + offset;
                catchPlayer = true;
                StartCoroutine(WaitToDie());
                
            }
            else if (other.gameObject.CompareTag("Ground"))
            {
                attackPlayer = false;
                touchedGround = true;
                
                
                
            }
        }

        IEnumerator WaitToDie()
        {
            audioSource.clip = sounds[Random.Range(0, sounds.Length)];
            audioSource.Play();
            
            yield return new WaitForSeconds(2f);
            
            GameManager.instance.GameOverBySpider();
            
        }


        void TouchedGround(Vector3 initPos)
        {
            transform.DetachChildren();
            spiderRb.useGravity = true;
            this.transform.rotation = new Quaternion(0, 180, 0, 0);

        }
    }
}