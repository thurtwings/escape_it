﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class Respawn : MonoBehaviour
    {
        #region Variables
	    
	#endregion



        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {

                RespawnManager.instance.lastCheckPointPosition = this.transform.position;
                GameManager.instance.timerBar.SetActive(true);
                RespawnManager.instance.timerLeftLevel = TimerLevel.instance.timeLeft;
                this.gameObject.SetActive(false);
                
            }
        }
    }
}