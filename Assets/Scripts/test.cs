﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class test : MonoBehaviour
{
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            rb.isKinematic = false;
            StartCoroutine(DestroyCube());
        }

    }
    
    IEnumerator DestroyCube()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(this.gameObject);
    }
}
