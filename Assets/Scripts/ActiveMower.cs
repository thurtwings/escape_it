﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class ActiveMower : MonoBehaviour
    {
        #region Variables
        public bool activated = false;
        Rigidbody rb;
        public float force;

        AudioSource audio;
        public float audioTime;
        #endregion



        private void Start()
        {
            rb = GetComponent<Rigidbody>();
            audio = GetComponent<AudioSource>();
        }
        void Update()
        {
            if (activated)
            {
                rb.AddForce(Vector3.right * force, ForceMode.Impulse);
                audio.Play();
                StartCoroutine(StopSound());
                activated = false;
            }
            
            

            
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                if (ThirdPersonPlayer.instance.hasTool)
                {
                    activated = true;
                    ThirdPersonPlayer.instance.hasTool = false;

                }
            }
        }
         IEnumerator StopSound()
        {
            yield return new WaitForSeconds(audioTime);
            audio.Stop();
        }
        
    }
}