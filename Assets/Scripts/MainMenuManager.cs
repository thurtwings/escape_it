﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ThurtwingsGames
{
    public class MainMenuManager : MonoBehaviour
    {
        #region Variables
        public GameObject creditPanel;
        public bool creditClicked;
	#endregion

        void Start()
        {
            creditPanel.SetActive(false);
            creditClicked = false;
        }

        public void OnClickStart()
        {
            SceneManager.LoadScene(1);
        }
        public void OnClickCredits()
        {
            creditClicked = !creditClicked;

            if (creditClicked)
            {
                creditPanel.SetActive(true);

            }

            else
            {
                creditPanel.SetActive(false);

            }
        }

        public void OnClickQuit()
        {
            Application.Quit();
        }
    }
}