﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class DontDestroy : MonoBehaviour
    {
        #region Variables
        public static DontDestroy instance;
	#endregion

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(instance);
            }
            else
            {
                Destroy(gameObject);
            }
            
        }
        private void Start()
        {
            RespawnManager.instance.originalPos = this.gameObject;
        }


        
    }
}