﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class OpenDoor : MonoBehaviour
    {
        #region Variables
        Animator animator;
        #endregion

        void Start()
        {
            animator = GetComponent<Animator>();
        }



        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                animator.SetTrigger("Open");
            }
        }
    }
}