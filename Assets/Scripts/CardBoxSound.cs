﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class CardBoxSound : MonoBehaviour
    {
        #region Variables
        AudioSource audioSource;
        public AudioClip[] clips;
        Rigidbody rb;
	#endregion

        void Start()
        {
            audioSource = GetComponent<AudioSource>();
            rb = GetComponent<Rigidbody>();
        }

    
        void Update()
        {
            if (rb.velocity.x == 0)
            {
                audioSource.Stop();
                audioSource.loop = false;
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                if (ThidPersonDash.instance.isDashing)
                {
                    audioSource.clip = clips[0];
                    audioSource.Play();
                }
                if(rb.velocity.x != 0)
                {
                    if ((ThirdPersonPlayer.instance.rb.velocity.x != 0 && ThirdPersonPlayer.instance.rb.velocity.y == 0 ||
                    ThirdPersonPlayer.instance.rb.velocity.z != 0 && ThirdPersonPlayer.instance.rb.velocity.y == 0)
                    && !ThidPersonDash.instance.isDashing)
                    {
                        audioSource.clip = clips[1];
                        audioSource.loop = true;
                        audioSource.Play();
                    }
                }
                
                
                
            }
            else
            {
                audioSource.Stop();
                audioSource.loop = false;
            }
        }
        
        private void OnCollisionExit(Collision collision)
        {
            if (!collision.gameObject.CompareTag("Player"))
            {
                audioSource.Stop();
                audioSource.loop = false;
            }
        }
    }
}