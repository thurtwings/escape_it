﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

namespace ThurtwingsGames
{
    public class TimerLevel : MonoBehaviour
    {
        #region Variables
        Image timerBar;
        public float maxTime;
        public float timeLeft;
        public Color high;
        public Color low;
        public static TimerLevel instance;
        #endregion

        void Start()
        {
            timerBar = GetComponent<Image>();
            
            instance = this;
            
            
            timeLeft = maxTime;

            
        }

       

        void Update()
        {
            if (GameManager.instance.hasWon)
            {
                timeLeft = maxTime;
            }
            if (timeLeft > 0)
            {
                timeLeft -= Time.deltaTime;
                timerBar.fillAmount = timeLeft / maxTime;
                timerBar.color = Color.Lerp(low, high, timerBar.fillAmount);
                
            }
            else
            {

                GameManager.instance.GameOverByTimer();
            }
            
            if(timeLeft < (maxTime - maxTime/5))
            {
                
                RenderSettings.fogStartDistance -=0.35f * Time.deltaTime;
            }
            
        }
    }
}