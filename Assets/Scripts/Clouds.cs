﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class Clouds : MonoBehaviour
    {
        #region Variables
        public float speed;
        Rigidbody rb;
	#endregion

        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

    
        void Update()
        {
            rb.AddForce(Vector3.up * speed * Time.deltaTime);
        }
    }
}