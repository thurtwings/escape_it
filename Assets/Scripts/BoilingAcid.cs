﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class BoilingAcid : MonoBehaviour
    {
        #region Variables
        public GameObject bubblePrefab;
        
	#endregion

        void Start()
        {
            
        }

        IEnumerator Boiling()
        {
            yield return new WaitForSeconds(Random.Range(0.01f, 0.1f));
            Debug.Log("spawn");
            GameObject cloneBubble = Instantiate(bubblePrefab, transform.position + new Vector3(Random.Range(-5, 5), 0, Random.Range(-2, 4)), Quaternion.Euler(new Vector3(0, 0, 0)));
            cloneBubble.transform.localScale -= new Vector3( 0.1f, 0.1f, 0.1f) * Time.deltaTime;
            Destroy(cloneBubble, .1f);
        }
        void Update()
        {
            StartCoroutine(Boiling());
        }
    }
}