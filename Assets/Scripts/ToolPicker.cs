﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames
{
    public class ToolPicker : MonoBehaviour
    {
        #region Variables
        Rigidbody rb;

	#endregion

        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                rb.isKinematic = true;

                this.transform.position = ThirdPersonPlayer.instance.toolPlace.transform.position;
                this.transform.parent = ThirdPersonPlayer.instance.toolPlace.transform;
                ThirdPersonPlayer.instance.hasTool = true;
            }
        }
    }
}